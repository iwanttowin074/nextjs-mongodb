import clientPromise from "../../lib/mongodb";

export default async (req, res) => {
    const client = await clientPromise;

    const db = client.db("movies")

    const movies = await db
        .collection("movies_metadata")
        .find({})
        .sort({ vote_average : -1 })
        .limit(20)
        .toArray();

    res.json(movies);
};